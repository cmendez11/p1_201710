import junit.framework.TestCase;
import model.data_structures.Queue;
import model.data_structures.Stack;


public class TestPila extends TestCase
{
	private Stack testpila;
	
	public void setupEscenario1( )
    {
        try
        {
        	testpila = new Stack( );
        }
        catch( Exception e )
        {
            fail( "No deber�a lanzar excepci�n." );
        }

    }
	public void setupEscenario2( )
    {
        try
        {
        	testpila = new Stack( );
        }
        catch( Exception e )
        {
            fail( "No deber�a lanzar excepci�n." );
        }
        testpila.push("a");
        testpila.push("b");
        testpila.push("c");
    }
	
	 public void testAgregar( )
	    {
	        setupEscenario1( );
	        
	        testpila.push("a");
	        testpila.push("b");
	        
	        String b=(String) testpila.pop();
	        assertEquals( "El nombre no es el esperado.", "b", b );
	        String a= (String) testpila.pop();
	       
	        assertEquals( "El nombre no es el esperado.", "a", a );
	      
	    }
	public void testvacio()
	{
		setupEscenario1( );
		boolean x=testpila.isEmpty();
		assertEquals( "Error en el tama�o",x );
		setupEscenario2( );
		 x=testpila.isEmpty();
		assertEquals( "Error en el tama�o",x );
		
	}
	public void testTamano()
	{
		setupEscenario1( );
		int x=testpila.size();
		assertEquals( "Error en el tama�o",0,x );
		setupEscenario2( );
		 x=testpila.size();
		assertEquals( "Error en el tama�o",2,x );
		
	}
}

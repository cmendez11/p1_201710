import junit.framework.TestCase;
import model.data_structures.Queue;


public class TestCola extends TestCase
{
	private Queue testcola;
	
	public void setupEscenario1( )
    {
        try
        {
        	testcola = new Queue( );
        }
        catch( Exception e )
        {
            fail( "No deber�a lanzar excepci�n." );
        }

    }
	public void setupEscenario2( )
    {
        try
        {
        	testcola = new Queue( );
        }
        catch( Exception e )
        {
            fail( "No deber�a lanzar excepci�n." );
        }
        testcola.enqueue("a");
        testcola.enqueue("b");
    }
	
	 public void testAgregar( )
	    {
	        setupEscenario1( );
	        
	        testcola.enqueue("a");
	        testcola.enqueue("b");
	        
	        String a=(String) testcola.dequeue();
	        String b= (String) testcola.dequeue();
	        assertEquals( "El nombre no es el esperado.", "a", a );
	        assertEquals( "El nombre no es el esperado.", "b", b );
	      
	    }
	 public void testvacio()
		{
			setupEscenario1( );
			boolean x=testcola.isEmpty();
			assertEquals( "Error en el tama�o",x );
			setupEscenario2( );
			 x=testcola.isEmpty();
			assertEquals( "Error en el tama�o",x );
			
		}
	 public void testTamano()
		{
			setupEscenario1( );
			int x=testcola.size();
			assertEquals( "Error en el tama�o",0,x );
			setupEscenario2( );
			 x=testcola.size();
			assertEquals( "Error en el tama�o",2,x );
			
		}
}

package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Queue<T> implements ICola<T>
{

	private ListaEncadenada l;

	public Queue()
	{
		l = new ListaEncadenada<>(); 
	}
	@Override
	public void enqueue(T item) 
	{
		l.agregarElementoFinal(item);
	}

	@Override
	public boolean isEmpty() 
	{
		if(l.darNumeroElementos() == 0)
		{
			return true;
		}
		return false;
	}

	@Override
	public T dequeue() 
	{
		if(l.darNumeroElementos() == 0)
		{
			throw  new EmptyStackException();
		}

		return (T) l.eliminarElemento(0);
	}

	@Override
	public int size() 
	{
		return l.darNumeroElementos();
	}

}
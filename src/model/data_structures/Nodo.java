package model.data_structures;

public class Nodo<T> 
{
	private Nodo<T> next;
	//private Nodo<T> previous;
	private T item;

	public Nodo(T it)
	{
		next = null;
		//	previous = null;
		item = it;
	}

	public Nodo<T> getNext()
	{
		return next;
	}
	//	public NodoDoble<T> getPrevious()
	//	{
	//		return previous;
	//	}
	public void setNext( Nodo<T> elem)
	{
		next = elem;
	}
	//	public void setPrevious( NodoDoble<T> elem)
	//	{
	//		previous = elem;
	//	}
	public T getItem()
	{
		return item;
	}
	public void setItem( T i)
	{
		item = i;
	}
	
	public Nodo<T> get(int pos)
	{
		if(pos == 0)
			return this;
		else
			return next.get(pos-1);
	}
}
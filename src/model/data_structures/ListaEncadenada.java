package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T>  {

	private Nodo<T> cabeza;
	private Nodo<T> actual;
	//private Nodo<T> anterioractual;
	private int listSize;

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>()
		{
			Nodo<T> act = null;

			public boolean hasNext()
			{
				if(listSize == 0)
					return false;
				if(act == null)
					return true;

				return act.getNext()!=null;
			}

			public T next()
			{
				if(act == null)
				{
					act = cabeza;
				}
				else
				{
					act = act.getNext();
				}
				return act.getItem();
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
	}
	public ListaEncadenada()
	{
		cabeza = null;
		listSize = 0;
		actual = null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		Nodo<T> nNodo = new Nodo<T>(elem);

		if(cabeza == null)
		{
			cabeza = nNodo;	
			actual = nNodo;
			//anterioractual = null;
		}
		else
		{
			//			Nodo<T>  actuala = cabeza;
			//			while(actuala.getNext() != null)
			//			{
			//				actuala = actuala.getNext();
			//			}
			//			actuala.setNext(nNodo);
			//			actual = actuala.getNext();
			actual.setNext(nNodo);
			//anterioractual = actual;
			actual = nNodo;
		}
		listSize++;
	}

	//	public void agregarElementoInicio(T elem) {
	//		// TODO Auto-generated method stub
	//		Nodo<T> nNodo = new Nodo<T>(elem);
	//
	//		if(cabeza == null)
	//		{
	//			cabeza = nNodo;	
	//			actual = nNodo;
	//		}
	//		else
	//		{
	//			nNodo.setNext(cabeza);
	//			cabeza = nNodo;
	//		}
	//		listSize++;
	//	}

	private void EliminarElementoInicio() 
	{
		Nodo<T> nNodo;
		if(cabeza.getNext() != null)
		{
			nNodo = new Nodo((T) cabeza.getNext());
			cabeza.setNext(null);
			cabeza = nNodo;
		}
		else
		{
			cabeza = null;
		}
		listSize--;
	}

	private void EliminarElementoFinal() 
	{

		if(cabeza.getNext() == null)
		{
			cabeza=null;
		}
		else
		{
			Nodo<T>  anter = actual;
			actual = cabeza;
			while(actual.getNext() != null)
			{
				if(actual.getNext() == anter)
				{
					actual.setNext(anter.getNext());
					anter.setNext(null);
				}
				else 
				{
					actual = actual.getNext();
				}
			}
		}	
		listSize--;

	}
	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Nodo<T>  actuala = cabeza;		
		int posi = 0;

		while(posi<pos && actuala.getNext() != null)
		{

			actuala = actuala.getNext();
			posi++;

		}

		return actuala.getItem();
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return listSize;
	}

	public T darCabeza()
	{
		return cabeza.getItem();
	}

	@Override
	public T eliminarElemento(int pos) 
	{
		// TODO Auto-generated method stub
		T elem = null;
		if(pos == 0)
		{
//			elem = cabeza.getItem();
//			EliminarElementoInicio();
			elem = cabeza.getItem();
			Nodo<T> x=cabeza.getNext();
			cabeza.setNext(null);
			cabeza=x;
			return elem;
		}
		else if(pos == listSize)
		{
			elem = actual.getItem();
			EliminarElementoFinal();
		}
		return elem;
	}
	
	public void cambiar(T elemento, int pos)
	{
		Nodo<T> nodo = cabeza.get(pos);
		nodo.setItem(elemento);
	}
	//	@Override
	//	public T darElementoPosicionActual() 
	//	{
	//		return  actual.getItem();
	//	}
	//
	//	@Override
	//	public boolean avanzarSiguientePosicion() {
	//		// TODO Auto-generated method stub
	//		boolean d = false;
	//		Nodo<T>  Anterior = new Nodo (actual);		
	//		if(Anterior != null && Anterior.getNext() != null)
	//		{
	//			Anterior = Anterior.getNext();
	//			d = true;
	//		}
	//
	//		return d;
	//	}
	//
	//	@Override
	//	public boolean retrocederPosicionAnterior() {
	//		// TODO Auto-generated method stub
	//		Nodo<T>  Anterior = new Nodo (actual);
	//		boolean a = false;
	//		actual = cabeza;
	//		while(actual.getNext() != null && actual.getNext() != Anterior && Anterior != actual)
	//		{
	//			actual = actual.getNext();
	//		}
	//		a = true;
	//
	//		return a;
	//
	//	}

}
package model.data_structures;

import java.util.Iterator;

public class Lista <T> implements ILista<T>  {

	private Nodo<T> list;
	private Nodo<T> actual;
	private int listSize;
	
	
	public Lista()
	{
		list = null;
		listSize = 0;
		actual = null;
	}



	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		Nodo<T> nNodo = new Nodo<T>(elem);
		nNodo.setNext(list);
		list=nNodo;
		
		/*if(list == null)
		{
			list = nNodo;	
			actual = nNodo;
		}
		else
		{
			Nodo<T>  actuala = list;
			while(actuala.getNext() != null)
			{
				
				actuala = actuala.getNext();
				
			}
			actuala.setNext(nNodo);
			actual = actuala.getNext();
		}*/
		listSize++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Nodo<T>  actuala = list;		
		int posi = 0;
		
		while(posi<pos && actuala.getNext() != null)
		{
			
			actuala = actuala.getNext();
			posi++;
			
		}

		return actuala.getItem();
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return listSize;
	}

	
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub

		return  actual.getItem();

	}

	
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean d = false;
		Nodo<T>  Anterior = new Nodo (actual);		
		if(Anterior != null && Anterior.getNext() != null)
		{
			Anterior = Anterior.getNext();
			d = true;
		}

		return d;
	}

	
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		Nodo<T>  Anterior = new Nodo (actual);
		boolean a = false;
		actual = list;
		while(actual.getNext() != null && actual.getNext() != Anterior && Anterior != actual)
		{
			actual = actual.getNext();
		}
		a = true;
		
		return a;
		
	}
	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		T x=darElemento(pos);
		Nodo<T>  actuala = list;
		while(actuala.getNext() != null)
		{
			
			actuala = actuala.getNext();
			
		}
		
		actual = actuala.getNext();
		return x;
	}



	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>()
				{
					Nodo<T> act = null;

					public boolean hasNext()
					{
						if(listSize == 0)
							return false;
						if(act == null)
							return true;

						return act.getNext()!=null;
					}

					public T next()
					{
						if(act == null)
						{
							act = list;
						}
						else
						{
							act = act.getNext();
						}
						return act.getItem();
					}

					@Override
					public void remove() {
						// TODO Auto-generated method stub
						
					}
				};
	}

	public void cambiar(T elemento, int pos)
	{
		Nodo<T> nodo = list.get(pos);
		nodo.setItem(elemento);
	}
}
package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.swing.tree.ExpandVetoException;

public class ArrayX<T> implements ILista<T>
{
	private T[] arr;
	private static int DEFAULT_SIZE=100;
	private int listsize=0;
	public ArrayX()
	{
		arr=(T[]) new Object[DEFAULT_SIZE];
	}
	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public T darElemento(int pos) {
		if(pos<0 || pos>=listsize)
		{
			throw new ArrayIndexOutOfBoundsException(pos);
		}
		return arr[pos];
	}
	
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return listsize;
	}
	
	public void agregarInicio(T item) {
		// TODO Auto-generated method stub
		if(listsize==arr.length)
		{
			expand();
		}
		for (int i=listsize;i>0;i--)
		{
			arr[i]=arr[i-1];
		}
		arr[0]=item;
		listsize++;
	}
	
	@Override
	public T eliminarElemento(int pos) {
		T x=null;
		if(pos<0 ||pos>=arr.length )
		{
			throw new ArrayIndexOutOfBoundsException(pos);
		}
		if(pos==listsize-1)
		{
			x=arr[pos];
			arr[pos]=null;			
		}
		else{
			for (int i=pos;pos<listsize-1;i++)
			{
				arr[i]=arr[i+1];
			}
			x=arr[pos];
			arr[listsize-1]=null;
		}
		
		listsize--;
		return x;
	}

	@Override
	public void agregarElementoFinal(Object elem) {
		// TODO Auto-generated method stub
		if(listsize==arr.length)
		{
			expand();
		}
		arr[listsize]=(T) elem;
		listsize++;
	}
	private void expand()
	{
		T[] exp=(T[]) (new Object[DEFAULT_SIZE+listsize]);
		for(int i =0;i<arr.length;i++)
		{
			exp[i]=arr[i];	
		}
		arr=null;
		arr=exp;
	}

	@Override
	public void cambiar(Object elemento, int pos) {
		// TODO Auto-generated method stub
		if(pos<0 || pos>=listsize)
		{
			throw new ArrayIndexOutOfBoundsException(pos);
		}
		arr[pos] = (T) elemento;
		
	}
}

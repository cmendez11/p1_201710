package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Stack<T> implements IPila<T> 
{
	private ListaEncadenada l;

	public Stack() 
	{
		// TODO Auto-generated constructor stub
		l = new ListaEncadenada<>();
	}
	@Override
	public void push (T item)  {

		l.agregarElementoFinal(item);
	}

	public T pop () 
	{		
		if(l.darNumeroElementos() == 0)
		{
			throw  new EmptyStackException();
		}
		return (T) l.eliminarElemento(l.darNumeroElementos());
	}

	public boolean isEmpty() 
	{
		if(l.darNumeroElementos() == 0)
		{
			return true;
		}
		return false;
	}

	public int size ()
	{
		return l.darNumeroElementos();
	}
}
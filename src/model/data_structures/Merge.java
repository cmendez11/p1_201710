package model.data_structures;

import java.util.Comparator;

import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;

public class Merge<T extends Comparable<T>>
{
	public void mergesortA(ArrayX<VOPelicula> arreglo, int tipo)
	{
		Object[] Objetos = new Object[arreglo.darNumeroElementos()];
		for(int i = 2; i/2 < arreglo.darNumeroElementos(); i = i*2)
		{
			for(int j = 0; j < arreglo.darNumeroElementos() && j + i/2 < arreglo.darNumeroElementos(); j += i)
			{
				int medio = j + i/2;

				int x;
				if(j+i > arreglo.darNumeroElementos())
				{
					x = arreglo.darNumeroElementos();
				}
				else
				{
					x =  j+i;
				}
				for(int  k = j, actualT = medio, a = j; k < medio || actualT < x; )
				{
					if(k == medio) 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
					else if(actualT == x)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else if(arreglo.darElemento(k).comparar(arreglo.darElemento(actualT), tipo) < 0)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
				}
				for(int h = j; h < x; h++)
				{
					//arreglo.darElemento(h) = (ArrayX<T>) Objetos[h];
					arreglo.cambiar(Objetos[h], h);
				}
			}
		}
	}

	public void mergesortB(ArrayX<VOTag> arreglo, int tipo)
	{
		Object[] Objetos = new Object[arreglo.darNumeroElementos()];
		for(int i = 2; i/2 < arreglo.darNumeroElementos(); i = i*2)
		{
			for(int j = 0; j < arreglo.darNumeroElementos() && j + i/2 < arreglo.darNumeroElementos(); j += i)
			{
				int medio = j + i/2;

				int x;
				if(j+i > arreglo.darNumeroElementos())
				{
					x = arreglo.darNumeroElementos();
				}
				else
				{
					x =  j+i;
				}
				for(int  k = j, actualT = medio, a = j; k < medio || actualT < x; )
				{
					if(k == medio) 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
					else if(actualT == x)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else if(arreglo.darElemento(k).comparar(arreglo.darElemento(actualT), tipo) < 0)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
				}
				for(int h = j; h < x; h++)
				{
					//arreglo.darElemento(h) = (ArrayX<T>) Objetos[h];
					arreglo.cambiar(Objetos[h], h);
				}
			}
		}
	}

	public void mergesortC(ArrayX<VORating> arreglo, int tipo)
	{
		Object[] Objetos = new Object[arreglo.darNumeroElementos()];
		for(int i = 2; i/2 < arreglo.darNumeroElementos(); i = i*2)
		{
			for(int j = 0; j < arreglo.darNumeroElementos() && j + i/2 < arreglo.darNumeroElementos(); j += i)
			{
				int medio = j + i/2;

				int x;
				if(j+i > arreglo.darNumeroElementos())
				{
					x = arreglo.darNumeroElementos();
				}
				else
				{
					x =  j+i;
				}
				for(int  k = j, actualT = medio, a = j; k < medio || actualT < x; )
				{
					if(k == medio) 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
					else if(actualT == x)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else if(arreglo.darElemento(k).comparar(arreglo.darElemento(actualT), tipo) < 0)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
				}
				for(int h = j; h < x; h++)
				{
					//arreglo.darElemento(h) = (ArrayX<T>) Objetos[h];
					arreglo.cambiar(Objetos[h], h);
				}
			}
		}
	}
	
	public void mergesortU(ArrayX<VOUsuario> arreglo, int tipo)
	{
		Object[] Objetos = new Object[arreglo.darNumeroElementos()];
		for(int i = 2; i/2 < arreglo.darNumeroElementos(); i = i*2)
		{
			for(int j = 0; j < arreglo.darNumeroElementos() && j + i/2 < arreglo.darNumeroElementos(); j += i)
			{
				int medio = j + i/2;

				int x;
				if(j+i > arreglo.darNumeroElementos())
				{
					x = arreglo.darNumeroElementos();
				}
				else
				{
					x =  j+i;
				}
				for(int  k = j, actualT = medio, a = j; k < medio || actualT < x; )
				{
					if(k == medio) 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
					else if(actualT == x)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else if(arreglo.darElemento(k).comparar(arreglo.darElemento(actualT), tipo) < 0)
					{
						Objetos[a++] = arreglo.darElemento(k++);
					}
					else 
					{
						Objetos[a++] = arreglo.darElemento(actualT++);
					}
				}
				for(int h = j; h < x; h++)
				{
					//arreglo.darElemento(h) = (ArrayX<T>) Objetos[h];
					arreglo.cambiar(Objetos[h], h);
				}
			}
		}
	}


	public void mergesort(ILista<VOPelicula> lista, int tipo)
	{

		Object[] objetos = new Object[lista.darNumeroElementos()];
		for(int i = 2; i/2 < lista.darNumeroElementos(); i = i*2)
		{
			for(int j = 0; j < lista.darNumeroElementos() && j+i/2 < lista.darNumeroElementos(); j += i)
			{
				int medio = j+i/2;
				int x;
				if(j+i > lista.darNumeroElementos())
				{
					x = lista.darNumeroElementos();
				}
				else
				{
					x = j+i;
				}

				for(int k = j, actualT = medio, a = j; k < medio || actualT < x; )
				{
					if(k == medio)
					{
						objetos[a++] = lista.darElemento(actualT++);
					}
					else if(actualT == x) 
					{
						objetos[a++] = lista.darElemento(k++);
					}
					else if(lista.darElemento(k).comparar(lista.darElemento(actualT),tipo) < 0)  
					{
						objetos[a++] = lista.darElemento(k++);
					}
					else 
					{
						objetos[a++] = lista.darElemento(actualT++);
					}
				}
				for(int h = j; h < x; h++)
				{
					lista.cambiar((VOPelicula) objetos[h] , h);
				}
			}
		}
		System.out.println("fallo");
	}
}

package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.data_structures.*;
import model.vo.*;
import api.ISistemaRecomendacionPeliculas;

public class CopyOfSistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{
	private Stack pilaPeliculas;
	private Queue colaRatings;
	private Queue colaTags;
	public ArrayX<VOPelicula> pelicula;
	public ArrayX<VORating> rating;
	public ArrayX<VOTag> tag;
	public Lista<VOGeneroPelicula> genePeli;
	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) 
	{

		pelicula=new ArrayX<VOPelicula>();
		genePeli=new Lista<>();
		boolean carg=false;
		BufferedReader peliculas;
		try {

			peliculas = new BufferedReader(new FileReader(rutaPeliculas));
			String linea =peliculas.readLine();
			//int d2=2;
			while(peliculas.ready())
			{
				linea = peliculas.readLine();
				String lie[] = linea.split(",");
				char[] x=linea.toCharArray();
				String id=lie[0];
				String Nombre="";		
				String A�o ="";
				String Genero=lie[lie.length-1];

				for(int i=0;i<x.length;i++)
				{
					if(x[i]=='(')
					{
						if(Character.isDigit(x[i+1]) && Character.isDigit(x[i+2]) && Character.isDigit(x[i+3]) && Character.isDigit(x[i+4]) )

						{
							String a=(""+x[i+1]);
							String b=(""+x[i+2]);
							String c=(""+x[i+3]);
							String d=(""+x[i+4]);
							A�o=""+a+b+c+d;
							i=x.length;
						}
					}
				}
				if(A�o!="")
				{
					String lie2[] = linea.split("\\("+A�o+"\\)");
					String lie3[] = lie2[0].split(",");
					for( int i=1;i<lie3.length;i++)
					{
						Nombre+=lie3[i];

					}
				}
				else
				{
					for( int i=1;i<lie.length-1;i++)
					{
						Nombre+=lie[i];

					}
				}

				if(Nombre.charAt(0)=='"')
				{

					Nombre=Nombre.substring(1);				
				}
				if(Nombre.charAt(Nombre.length()-1)=='"')
				{				
					Nombre=Nombre.substring(0,Nombre.length()-1);	
				}




				int a=0;
				if(A�o!="")
				{
					a=Integer.parseInt(A�o);

				}	
				VOPelicula peli =new VOPelicula();				
				peli.setIdUsuario(new Long(Long.parseLong(id)));
				peli.setAgnoPublicacion(a);				
				peli.setTitulo(Nombre);
				pelicula.agregarElementoFinal(peli);

				String[] gene=Genero.split("\\|");
				Lista<String> Gener = new Lista<String>();

				for(int i =0;i<=gene.length-1;i++)
				{
					Gener.agregarElementoFinal(gene[i]);

				}
				pelicula.darElemento(0).setGenerosAsociados(Gener);

				//d2++;

			}
			peliculas.close();
			carg=true;
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return carg;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		boolean carg=false;

		rating=new ArrayX();
		BufferedReader ratingsReader;
		try {
			ratingsReader = new BufferedReader(new FileReader(rutaRatings));
			String linea =ratingsReader.readLine();
			//int d2=2;
			while(ratingsReader.ready())
			{
				linea = ratingsReader.readLine();
				String lie[] = linea.split(",");
				String userid=lie[0];
				String movieid=lie[1];
				String rat=lie[2];
				String timestamp=lie[3];
				VORating x=new VORating();
				
				x.setIdPelicula(new Long(Long.parseLong(movieid)));
				x.setIdUsuario(new Long(Long.parseLong(userid)));
				x.setRating(Double.parseDouble(rat));
				x.setTimestamp(new Long(Long.parseLong(timestamp)));
				rating.agregarElementoFinal(x);
				carg=true;
			} 
		}

		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		} 
		return carg;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		boolean carg=false;

		tag= new ArrayX();
		BufferedReader tagReader;
		try {
			tagReader = new BufferedReader(new FileReader(rutaTags));
			String linea =tagReader.readLine();
			//int d2=2;
			while(tagReader.ready())
			{
				linea = tagReader.readLine();
				String lie[] = linea.split(",");
				String userid=lie[0];
				String movieid=lie[1];
				String t="";
				for(int i=2;i<lie.length-1;i++){
					t+=lie[i];
				}
				String timestamp=lie[lie.length-1];

				VOTag x=new VOTag();
				
				x.setTag(t);
				x.setTimestamp(new Long(Long.parseLong(timestamp)));
				x.setIdPelicula(new Long(Long.parseLong(movieid)));
				tag.agregarElementoFinal(x);
				carg=true;
			} 
		}

		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		} 
		return carg;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return pelicula.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tag.darNumeroElementos();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) 
	{
		// TODO Auto-generated method stub
		Lista<VOGeneroPelicula> aRetorn=new Lista<VOGeneroPelicula>();
		for(int i=0;i<pelicula.darNumeroElementos();i++)
		{
			VOPelicula peli=pelicula.darElemento(i);
			for(int d=0;d<pelicula.darElemento(i).getGenerosAsociados().darNumeroElementos();d++)
			{
				String nGene=pelicula.darElemento(i).getGenerosAsociados().darElemento(d);

				if(genePeli.darNumeroElementos()==0)
				{

					VOGeneroPelicula nGenPeli=new VOGeneroPelicula();
					nGenPeli.setGenero(nGene);
					Lista<VOPelicula> y=new Lista<VOPelicula>();
					y.agregarElementoFinal(peli);
					nGenPeli.setPeliculas(y);
					genePeli.agregarElementoFinal(nGenPeli);
				}
				else
				{
					boolean ll=false;
					for(int c=0;c<genePeli.darNumeroElementos();c++)
					{



						if(nGene.equalsIgnoreCase(genePeli.darElemento(c).getGenero()))
						{
							ILista<VOPelicula> y=genePeli.darElemento(c).getPeliculas();							
							y.agregarElementoFinal(peli);
							genePeli.darElemento(c).setPeliculas(y);
							c=genePeli.darNumeroElementos();
							ll=true;
						}

					}
					if(ll==false)
					{					
						VOGeneroPelicula nGenPeli=new VOGeneroPelicula();
						nGenPeli.setGenero(nGene);
						Lista<VOPelicula> y=new Lista<VOPelicula>();
						y.agregarElementoFinal(peli);
						nGenPeli.setPeliculas(y);
						genePeli.agregarElementoFinal(nGenPeli);


					}
				}
			}
		}
		// ordenar por fecha
		
		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{
			Lista<VOPelicula> x= (Lista<VOPelicula>) genePeli.darElemento(i).getPeliculas();
			VOGeneroPelicula genConPeliculas=new VOGeneroPelicula();
			Lista<VOPelicula> pelis=new Lista<VOPelicula>();
			genConPeliculas.setGenero(genePeli.darElemento(i).getGenero());
			for(int l=x.darNumeroElementos()-n;l<x.darNumeroElementos();l++)
			{
				if(x.darNumeroElementos()-n<=l)
				{
					pelis.agregarElementoFinal(x.darElemento(l));
				}

			}
			genConPeliculas.setPeliculas(pelis);
			aRetorn.agregarElementoFinal(genConPeliculas);
		}
		return aRetorn;
	}

	@Override
	public ArrayX<VOPelicula> catalogoPeliculasOrdenadoSR() {
		ArrayX<VOPelicula> aReto=pelicula;
		//orden por raiting
		//orden por a�o
		//orden por titulo

		return aReto;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() 
	{
		Lista<VOGeneroPelicula> aRetorn=new Lista<VOGeneroPelicula>();
		//ordenar por promedioraiting
		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{
			Lista<VOPelicula> x= (Lista<VOPelicula>) genePeli.darElemento(i).getPeliculas();
			VOGeneroPelicula genConPeliculas=new VOGeneroPelicula();
			Lista<VOPelicula> pelis=new Lista<VOPelicula>();
			genConPeliculas.setGenero(genePeli.darElemento(i).getGenero());
			int sum=0;
			for(int l=0;l<x.darNumeroElementos();l++)
			{
				sum+=x.darElemento(l).getPromedioRatings();
			}
			int promedio=(sum/x.darNumeroElementos());
			for(int l=0;l<x.darNumeroElementos();l++)
			{
				if(promedio<=x.darElemento(l).getPromedioRatings())
				{
					pelis.agregarElementoFinal(x.darElemento(l));
				}
			}
			genConPeliculas.setPeliculas(pelis);
			aRetorn.agregarElementoFinal(genConPeliculas);
		}
		return aRetorn;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() 
	{
		Lista<VOGeneroPelicula> aRetorn=new Lista<VOGeneroPelicula>();
		if(genePeli.darNumeroElementos()==0)
		{
			for(int i=0;i<pelicula.darNumeroElementos();i++)
			{
				VOPelicula peli=pelicula.darElemento(i);
				for(int d=0;d<pelicula.darElemento(i).getGenerosAsociados().darNumeroElementos();d++)
				{
					String nGene=pelicula.darElemento(i).getGenerosAsociados().darElemento(d);

					if(genePeli.darNumeroElementos()==0)
					{

						VOGeneroPelicula nGenPeli=new VOGeneroPelicula();
						nGenPeli.setGenero(nGene);
						Lista<VOPelicula> y=new Lista<VOPelicula>();
						y.agregarElementoFinal(peli);
						nGenPeli.setPeliculas(y);
						genePeli.agregarElementoFinal(nGenPeli);
					}
					else
					{
						boolean ll=false;
						for(int c=0;c<genePeli.darNumeroElementos();c++)
						{



							if(nGene.equalsIgnoreCase(genePeli.darElemento(c).getGenero()))
							{
								ILista<VOPelicula> y=genePeli.darElemento(c).getPeliculas();							
								y.agregarElementoFinal(peli);
								genePeli.darElemento(c).setPeliculas(y);
								c=genePeli.darNumeroElementos();
								ll=true;
							}

						}
						if(ll==false)
						{					
							VOGeneroPelicula nGenPeli=new VOGeneroPelicula();
							nGenPeli.setGenero(nGene);
							Lista<VOPelicula> y=new Lista<VOPelicula>();
							y.agregarElementoFinal(peli);
							nGenPeli.setPeliculas(y);
							genePeli.agregarElementoFinal(nGenPeli);


						}
					}
				}	
			}
			//ordenar	
		}
		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{
			Lista<VOPelicula> x= (Lista<VOPelicula>) genePeli.darElemento(i).getPeliculas();
			VOGeneroPelicula genConPeliculas=new VOGeneroPelicula();
			Lista<VOPelicula> pelis=new Lista<VOPelicula>();
			genConPeliculas.setGenero(genePeli.darElemento(i).getGenero());
			for(int l=0;l<x.darNumeroElementos();l++)
			{	
				pelis.agregarElementoFinal(x.darElemento(l));
			}
			genConPeliculas.setPeliculas(pelis);
			aRetorn.agregarElementoFinal(genConPeliculas);
		}
		return aRetorn;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		//ordenar raiting por pelicula
		Lista<VORating> aRetor=new Lista<VORating>();
		for(int i=0;i<rating.darNumeroElementos();i++)
		{
			if(rating.darElemento(i).getIdPelicula()==idPelicula)
			{
				aRetor.agregarElementoFinal(rating.darElemento(i));
			}
			if(rating.darElemento(i).getIdPelicula()>idPelicula)
			{
				i=rating.darNumeroElementos();
			}
		}
		return aRetor;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		//ordenar raiting por usuarios
		long usuarioanterior=0;
		int numRait=1;
		long time=0;
		Lista<VOUsuario> aREtor=new Lista<VOUsuario>();
		VOUsuario us=new VOUsuario();
		for(int i=0;i<rating.darNumeroElementos();i++)
		{
			if(usuarioanterior==rating.darElemento(i).getIdUsuario())
			{
				numRait++;
				aREtor.darElemento(0).setNumRatings(numRait);	
			}
			else
			{
				us=new VOUsuario();
				usuarioanterior=rating.darElemento(i).getIdUsuario();
				time=rating.darElemento(i).getTimestamp();
				us.setIdUsuario(usuarioanterior);	
				us.setPrimerTimestamp(time);
				us.setNumRatings(numRait);
				numRait=1;
				aREtor.agregarElementoFinal(us);
				
				
			}
			usuarioanterior=rating.darElemento(i).getIdUsuario();	
		}
		
		return null;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}

}

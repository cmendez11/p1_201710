package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.data_structures.*;
import model.vo.*;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{
	private Stack pilaPeliculas;
	private Queue colaRatings;
	private Queue colaTags;
	public ArrayX<VOPelicula> pelicula;
	public ArrayX<VORating> rating;
	public ArrayX<VOTag> tag;
	public ArrayX<VOGeneroPelicula> genePeli;
	public Stack<VOOperacion> pila = new Stack<VOOperacion>();

	Stack  xd = new Stack();

	ArrayX<VOOperacion> arreglo= new ArrayX<VOOperacion>();
	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) 
	{
		pelicula=new ArrayX<VOPelicula>();

		boolean carg=false;
		BufferedReader peliculas;
		try {

			peliculas = new BufferedReader(new FileReader(rutaPeliculas));
			String linea =peliculas.readLine();
			//int d2=2;
			while(peliculas.ready())
			{
				linea = peliculas.readLine();
				String lie[] = linea.split(",");
				char[] x=linea.toCharArray();
				String id=lie[0];
				String Nombre="";		
				String A�o ="";
				String Genero=lie[lie.length-1];

				for(int i=0;i<x.length;i++)
				{
					if(x[i]=='(')
					{
						if(Character.isDigit(x[i+1]) && Character.isDigit(x[i+2]) && Character.isDigit(x[i+3]) && Character.isDigit(x[i+4]) )

						{
							String a=(""+x[i+1]);
							String b=(""+x[i+2]);
							String c=(""+x[i+3]);
							String d=(""+x[i+4]);
							A�o=""+a+b+c+d;
							i=x.length;
						}
					}
				}
				if(A�o!="")
				{
					String lie2[] = linea.split("\\("+A�o+"\\)");
					String lie3[] = lie2[0].split(",");
					for( int i=1;i<lie3.length;i++)
					{
						Nombre+=lie3[i];

					}
				}
				else
				{
					for( int i=1;i<lie.length-1;i++)
					{
						Nombre+=lie[i];

					}
				}

				if(Nombre.charAt(0)=='"')
				{

					Nombre=Nombre.substring(1);				
				}
				if(Nombre.charAt(Nombre.length()-1)=='"')
				{				
					Nombre=Nombre.substring(0,Nombre.length()-1);	
				}




				int a=0;
				if(A�o!="")
				{
					a=Integer.parseInt(A�o);

				}	
				VOPelicula peli =new VOPelicula();				
				peli.setIdUsuario(new Long(Long.parseLong(id)));
				peli.setAgnoPublicacion(a);				
				peli.setTitulo(Nombre);
				pelicula.agregarElementoFinal(peli);

				String[] gene=Genero.split("\\|");
				Lista<String> Gener = new Lista<String>();

				for(int i =0;i<=gene.length-1;i++)
				{
					Gener.agregarElementoFinal(gene[i]);

				}


				pelicula.darElemento(pelicula.darNumeroElementos()-1).setGenerosAsociados(Gener);;

				//d2++;

			}
			peliculas.close();
			carg=true;
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return carg;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		boolean carg=false;

		rating=new ArrayX<>();
		BufferedReader ratingsReader;
		try {
			ratingsReader = new BufferedReader(new FileReader(rutaRatings));
			String linea =ratingsReader.readLine();
			//int d2=2;
			while(ratingsReader.ready())
			{
				linea = ratingsReader.readLine();
				String lie[] = linea.split(",");
				String userid=lie[0];
				String movieid=lie[1];
				String rat=lie[2];
				String timestamp=lie[3];
				VORating x=new VORating();
				x.setIdPelicula(new Long(Long.parseLong(movieid)));
				x.setIdUsuario(new Long(Long.parseLong(userid)));
				x.setRating(Double.parseDouble(rat));
				x.setTimestamp(new Long(Long.parseLong(timestamp)));
				rating.agregarElementoFinal(x);
				carg=true;
			} 
		}

		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		} 
		return carg;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		boolean carg=false;

		tag=new ArrayX<>();
		BufferedReader tagReader;
		try {
			tagReader = new BufferedReader(new FileReader(rutaTags));
			String linea =tagReader.readLine();
			//int d2=2;
			while(tagReader.ready())
			{
				linea = tagReader.readLine();
				String lie[] = linea.split(",");
				String userid=lie[0];
				String movieid=lie[1];
				String t="";
				for(int i=2;i<lie.length-1;i++){
					t+=lie[i];
				}
				String timestamp=lie[lie.length-1];

				VOTag x=new VOTag();
				x.setTag(t);
				x.setTimestamp(new Long(Long.parseLong(timestamp)));
				x.setIdPelicula(new Long(Long.parseLong(movieid)));
				tag.agregarElementoFinal(x);
				carg=true;
			} 
		}

		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		} 
		return carg;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return pelicula.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tag.darNumeroElementos();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		a�adirRaitingPelicula();
		Merge R = new Merge();
		R.mergesortA(pelicula, 1);
		Lista<VOGeneroPelicula> aRetorn=new Lista<VOGeneroPelicula>();
		cargaVOgenePeli();
		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{

			Lista<VOPelicula> x= (Lista<VOPelicula>) genePeli.darElemento(i).getPeliculas();
			VOGeneroPelicula genConPeliculas=new VOGeneroPelicula();
			Lista<VOPelicula> pelis=new Lista<VOPelicula>();
			genConPeliculas.setGenero(genePeli.darElemento(i).getGenero());

			for(int l=0;l<n;l++)
			{

				pelis.agregarElementoFinal(x.darElemento(l));

			}
			genConPeliculas.setPeliculas(pelis);
			aRetorn.agregarElementoFinal(genConPeliculas);
		}
		return aRetorn;
	}
	private void a�adirRaitingPelicula()
	{
		Merge R = new Merge();
		R.mergesortC(rating, 0);
		R.mergesortA(pelicula, 7);
		int posenR=0;
		double s=0;
		int numRait=0;
		double prom= numRait==0? s:s/numRait;

		for(int i=0;i<pelicula.darNumeroElementos();i++)
		{
			for(int l=posenR;l<rating.darNumeroElementos();l++)
			{
				if(rating.darElemento(l).getIdPelicula()==pelicula.darElemento(i).getIdUsuario())
				{
					s+=rating.darElemento(l).getRating();
					numRait++;
				}
				else
				{
					posenR=l;
					l=rating.darNumeroElementos();

				}
			}
			prom= numRait==0? s:s/numRait;
			pelicula.darElemento(i).setNumeroRatings(numRait);	
			pelicula.darElemento(i).setPromedioRatings(prom);
			s=0;
			numRait=0;

		}
	}
	private void anadirTagspeli()
	{
		Merge R = new Merge();
		R.mergesortB(tag, 0);
		
		int posenR=0;
		
		int numT=0;
		

		for(int i=0;i<pelicula.darNumeroElementos();i++)
		{
			ArrayX tags=new ArrayX<>();
			for(int l=posenR;l<tag.darNumeroElementos();l++)
			{
				if(tag.darElemento(l).getIdPelicula()==pelicula.darElemento(i).getIdUsuario())
				{	
					numT++;
					tags.agregarElementoFinal(tag.darElemento(l).getTag());
				}
				else
				{
					posenR=l;
					l=rating.darNumeroElementos();
					
				}
			}	
			pelicula.darElemento(i).setTagsAsociados(tags);
			pelicula.darElemento(i).setNumeroTags(numT);
			numT=0;

		}
	}
	private void cargaVOgenePeli()
	{
		genePeli=new ArrayX<>();
		for(int i=0;i<pelicula.darNumeroElementos();i++)
		{
			VOPelicula peli=pelicula.darElemento(i);
			for(int d=0;d<pelicula.darElemento(i).getGenerosAsociados().darNumeroElementos();d++)
			{
				String nGene=pelicula.darElemento(i).getGenerosAsociados().darElemento(d);

				if(genePeli.darNumeroElementos()==0)
				{

					VOGeneroPelicula nGenPeli=new VOGeneroPelicula();
					nGenPeli.setGenero(nGene);
					Lista<VOPelicula> y=new Lista<VOPelicula>();
					y.agregarElementoFinal(peli);
					nGenPeli.setPeliculas(y);
					genePeli.agregarElementoFinal(nGenPeli);
				}
				else
				{
					boolean ll=false;
					for(int c=0;c<genePeli.darNumeroElementos();c++)
					{



						if(nGene.equalsIgnoreCase(genePeli.darElemento(c).getGenero()))
						{
							ILista<VOPelicula> y=genePeli.darElemento(c).getPeliculas();							
							y.agregarElementoFinal(peli);
							genePeli.darElemento(c).setPeliculas(y);
							c=genePeli.darNumeroElementos();
							ll=true;
						}

					}
					if(ll==false)
					{					
						VOGeneroPelicula nGenPeli=new VOGeneroPelicula();
						nGenPeli.setGenero(nGene);
						Lista<VOPelicula> y=new Lista<VOPelicula>();
						y.agregarElementoFinal(peli);
						nGenPeli.setPeliculas(y);
						genePeli.agregarElementoFinal(nGenPeli);


					}
				}
			}
		}


	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		ArrayX<VOPelicula> aReto=pelicula;
		Merge R = new Merge();

		R.mergesortA(aReto, 0);
		R.mergesortA(aReto, 3);
		R.mergesortA(aReto, 4);
		return aReto;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		Lista<VOGeneroPelicula> aRetorn=new Lista<VOGeneroPelicula>();
		//ordenar por promedioraiting
		a�adirRaitingPelicula();
		Merge R = new Merge();
		R.mergesortA(pelicula, 3);	
		cargaVOgenePeli();
		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{
			Lista<VOPelicula> x= (Lista<VOPelicula>) genePeli.darElemento(i).getPeliculas();
			VOGeneroPelicula genConPeliculas=new VOGeneroPelicula();
			Lista<VOPelicula> pelis=new Lista<VOPelicula>();
			genConPeliculas.setGenero(genePeli.darElemento(i).getGenero());
			int sum=0;
			for(int l=0;l<x.darNumeroElementos();l++)
			{
				sum+=x.darElemento(l).getPromedioRatings();
			}
			int promedio=(sum/x.darNumeroElementos());
			for(int l=0;l<x.darNumeroElementos();l++)
			{
				if(promedio<=x.darElemento(l).getPromedioRatings())
				{
					pelis.agregarElementoFinal(x.darElemento(l));
				}
			}
			genConPeliculas.setPeliculas(pelis);
			aRetorn.agregarElementoFinal(genConPeliculas);
		}
		return aRetorn;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		Lista<VOGeneroPelicula> aRetorn=new Lista<VOGeneroPelicula>();

		Merge R = new Merge();
		R.mergesortA(pelicula, 4);
		cargaVOgenePeli();

		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{
			Lista<VOPelicula> x= (Lista<VOPelicula>) genePeli.darElemento(i).getPeliculas();
			VOGeneroPelicula genConPeliculas=new VOGeneroPelicula();
			Lista<VOPelicula> pelis=new Lista<VOPelicula>();
			genConPeliculas.setGenero(genePeli.darElemento(i).getGenero());
			for(int l=0;l<x.darNumeroElementos();l++)
			{	
				pelis.agregarElementoFinal(x.darElemento(l));
			}
			genConPeliculas.setPeliculas(pelis);
			aRetorn.agregarElementoFinal(genConPeliculas);
		}
		return aRetorn;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		Merge R = new Merge();
		R.mergesortC(rating, 0);

		ArrayX<VORating> aRetor=new ArrayX<VORating>();
		for(int i=0;i<rating.darNumeroElementos();i++)
		{
			if(rating.darElemento(i).getIdPelicula()==idPelicula)
			{
				aRetor.agregarElementoFinal(rating.darElemento(i));
			}
			if(rating.darElemento(i).getIdPelicula()>idPelicula)
			{
				i=rating.darNumeroElementos();
			}
		}
		R.mergesortC(aRetor, 0);
		return aRetor;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		Merge R = new Merge();
		ArrayX nn=new ArrayX();
		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{	ArrayX<VOPelicula> x=new ArrayX<VOPelicula>();
			Lista lis= (Lista) genePeli.darElemento(i).getPeliculas();
			ArrayX<VOPelicula> xx=new ArrayX<VOPelicula>();
			for(int o=0;o<lis.darNumeroElementos();o++)
			{
				xx.agregarElementoFinal(lis.darElemento(o));				
			}
			R.mergesortA(xx,7);
			R.mergesortC(rating, 1);
			long usuarioanterior=0;
			int numRait=1;
			ArrayX<VOUsuarioConteo> aREtor=new ArrayX<VOUsuarioConteo>();
			VOUsuarioConteo us=new VOUsuarioConteo();
			for(int o=0;o<rating.darNumeroElementos();o++)
			{
				if(busquedaBinaria(xx, rating.darElemento(o).getIdPelicula())==-1)
				continue;
				if(usuarioanterior==rating.darElemento(o).getIdUsuario())
				{
					numRait++;
					aREtor.darElemento(0).setConteo(numRait);	
				}
				else
				{
					us=new VOUsuarioConteo();
					usuarioanterior=rating.darElemento(o).getIdUsuario();					
					us.setIdUsuario(usuarioanterior);
					us.setConteo(numRait);
					numRait=1;
					aREtor.agregarElementoFinal(us);
				}
				usuarioanterior=rating.darElemento(o).getIdUsuario();	
			}
			VOGeneroUsuario nuevoUsConteo=new VOGeneroUsuario();
			nuevoUsConteo.setGenero(genePeli.darElemento(i).getGenero());
			nuevoUsConteo.setUsuarios(aREtor);
			nn.agregarElementoFinal(nuevoUsConteo);
		}
		for(int i=0;i<nn.darNumeroElementos();i++)
		{
			
		}
		return nn;
	}
	private static int busquedaBinaria(ArrayX x, Long dato){
		  int n = x.darNumeroElementos();
		  int centro,inf=0,sup=n-1;
		   while(inf<=sup){
		     centro=(sup+inf)/2;
		     if(((VOPelicula)x.darElemento(centro)).getIdUsuario()==dato) return centro;
		     else if(dato < ((VOPelicula)x.darElemento(centro)).getIdUsuario() ){
		        sup=centro-1;
		     }
		     else {
		       inf=centro+1;
		     }
		   }
		   return -1;
		 }
	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		//ordenar 
		ArrayX aRet=cargaUsuarios();
		Merge m= new Merge<>();
		m.mergesortU(aRet, 0);
		m.mergesortU(aRet, 2);
		m.mergesortU(aRet, 1);
		return aRet;
	}
	private ArrayX<VOUsuario> cargaUsuarios()
	{
		Merge R = new Merge();
		R.mergesortC(rating, 1);
				long usuarioanterior=0;
				int numRait=1;
				long time=0;
				ArrayX<VOUsuario> aREtor=new ArrayX<VOUsuario>();
				VOUsuario us=new VOUsuario();
				for(int i=0;i<rating.darNumeroElementos();i++)
				{
					if(usuarioanterior==rating.darElemento(i).getIdUsuario())
					{
						numRait++;
						aREtor.darElemento(0).setNumRatings(numRait);	
					}
					else
					{
						us=new VOUsuario();
						usuarioanterior=rating.darElemento(i).getIdUsuario();
						time=rating.darElemento(i).getTimestamp();
						us.setIdUsuario(usuarioanterior);	
						us.setPrimerTimestamp(time);
						us.setNumRatings(numRait);
						numRait=1;
						aREtor.agregarElementoFinal(us);
						
						
					}
					usuarioanterior=rating.darElemento(i).getIdUsuario();	
				}
				return aREtor;
	}
	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		anadirTagspeli();
		cargaVOgenePeli();
		Merge R = new Merge();
		R.mergesortA(pelicula, 5);
		Lista aReto=new Lista<>();
		
		for(int i=0;i<genePeli.darNumeroElementos();i++)
		{	ArrayX<VOPelicula> x=new ArrayX<VOPelicula>();
			Lista lis= (Lista) genePeli.darElemento(i).getPeliculas();
			ArrayX<VOPelicula> xx=new ArrayX<VOPelicula>();
			for(int o=0;o<lis.darNumeroElementos();o++)
			{
				xx.agregarElementoFinal(lis.darElemento(o));				
			}
			R.mergesortA(xx,2);
			for(int o=xx.darNumeroElementos()-n;o<xx.darNumeroElementos();o++)
			{
				x.agregarElementoFinal(xx.darElemento(o));			
			}
			VOGeneroPelicula gen=new VOGeneroPelicula();
			gen.setGenero(genePeli.darElemento(i).getGenero());
			gen.setPeliculas(x);
			aReto.agregarElementoFinal(gen);
		}
		return aReto;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		Lista<VOUsuarioGenero> aRetorn=new Lista<VOUsuarioGenero>();
		ArrayX<VOUsuario> usuario = new ArrayX<VOUsuario>();
		for (int i = 0; i < aRetorn.darNumeroElementos(); i++) 
		{
			usuario.agregarElementoFinal(aRetorn.darElemento(i).getIdUsuario());
		}
		Merge a = new Merge();
		//a.mergesortU(aRetorn, 0);
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		anadirTagspeli();
		Merge R = new Merge();
		R.mergesortB(tag, 0);

		ArrayX<VOTag> aRetor=new ArrayX<VOTag>();

		for(int i=0;i<tag.darNumeroElementos();i++)
		{
			if(tag.darElemento(i).getIdPelicula() == idPelicula)
			{
				aRetor.agregarElementoFinal(tag.darElemento(i));
			}
			if(tag.darElemento(i).getIdPelicula()>idPelicula)
			{
				i = tag.darNumeroElementos();
			}
		}

		R.mergesortB(aRetor, 0);

		return aRetor;
	}
	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) 
	{
		// TODO Auto-generated method stub
		VOOperacion xd = new VOOperacion();
		xd.setOperacion(nomOperacion);
		xd.setTimestampInicio(tinicio);
		xd.setTimestampFin(tfin);
		pila.push(xd);
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		int a = 0;
		while(a < pila.size())
		{
			arreglo.agregarElementoFinal((pila.pop()).getOperacion());
			a++;
		}
		return (ILista<VOOperacion>) arreglo;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub
		for(int i = 0; i < pila.size(); i++)
		{
			pila.pop();
		}

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		for(int i = 0; i < n; i++)
		{
			xd.push(pila.pop());
		}
		return (ILista<VOOperacion>) xd;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		for(int i = 0; i < n; i++)
		{
			pila.pop();
		}
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}

}

package model.vo;

import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>
{

	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;

	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;



	public long getIdUsuario() {
		return idPelicula;
	}
	public void setIdUsuario(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	@Override
	public int compareTo(VOPelicula o) {
		// TODO Auto-generated method stub

		return 0;
	}

	public int comparar(VOPelicula o, int tipo)
	{
		int a = 0;

		if(tipo == 0)
		{
			a = titulo.compareToIgnoreCase(o.getTitulo());
		}
		else if(tipo == 1)
		{
			if(numeroRatings < o.getNumeroRatings())
				a=-1;
			else if(numeroRatings > o.getNumeroRatings())
				a=1;
		}
		else if(tipo == 2)
		{
			if(numeroTags < o.getNumeroTags())
				a=-1;
			else if(numeroTags > o.getNumeroTags())
				a=1;
		}
		else if(tipo == 3)
		{
			if(promedioRatings < o.getPromedioRatings())
				a=-1;
			else if(promedioRatings > o.getPromedioRatings())
				a=1;
		}
		else if(tipo == 4)
		{
			if(agnoPublicacion < o.getAgnoPublicacion())
				a=-1;
			else if(agnoPublicacion > o.getAgnoPublicacion())
				a=1;
		}
		else if(tipo == 5)
		{
			if(tagsAsociados.darNumeroElementos() < o.getTagsAsociados().darNumeroElementos())
				a=-1;
			else if(tagsAsociados.darNumeroElementos() > o.getTagsAsociados().darNumeroElementos())
				a=1;
		}
		else if(tipo == 6)
		{
			if(generosAsociados.darNumeroElementos() < o.getGenerosAsociados().darNumeroElementos())
				a=-1;
			else if(generosAsociados.darNumeroElementos() > o.getGenerosAsociados().darNumeroElementos())
				a=1;
		}
		else if(tipo == 7)
		{
			if(idPelicula < o.idPelicula)
				a=-1;
			else if(idPelicula > o.idPelicula)
				a=1;
		}
		return a;
	}



}

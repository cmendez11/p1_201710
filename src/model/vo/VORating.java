package model.vo;

public class VORating implements Comparable<VORating>{

	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	//
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public int compareTo(VORating o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int comparar(VORating o, int tipo)
	{
		int a = 0;

		if(tipo == 0)
		{
			if(idPelicula < o.getIdPelicula())
				a=-1;
			else if(idPelicula > o.getIdPelicula())
				a=1;
		}
		else if(tipo == 1)
		{
			if(idUsuario < o.getIdUsuario())
				a=-1;
			else if(idUsuario > o.getIdUsuario())
				a=1;
		}
		else if(tipo ==2)
		{
			if(rating < o.getRating())
				a=-1;
			else if(rating > o.getRating())
				a=1;
		}
		else if(tipo ==2)
		{
			if(timestamp < o.getTimestamp())
				a=-1;
			else if(timestamp > o.getTimestamp())
				a=1;
		}
		return a;
	}


}

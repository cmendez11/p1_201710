package model.vo;

public class VOUsuario implements Comparable<VOUsuario>
{

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private double diferenciaOpinion;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	@Override
	public int compareTo(VOUsuario arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int comparar(VOUsuario o, int tipo)
	{
		int a = 0;

		if(tipo == 0)
		{
			if(idUsuario < o.getIdUsuario())
				a=-1;
			else if(idUsuario > o.getIdUsuario())
				a=1;
		}
		else if(tipo == 1)
		{
			if(primerTimestamp < o.getPrimerTimestamp())
				a =-1;
			else if(primerTimestamp > o.getPrimerTimestamp())
				a=1;
		}
		else if(tipo == 2)
		{
			if(numRatings < o.getNumRatings())
				a=-1;
			else if(numRatings > o.getNumRatings())
				a=1;
		}
		else if(tipo ==3)
		{
			if(diferenciaOpinion < o.getDiferenciaOpinion())
				a=-1;
			else if(diferenciaOpinion > o.getDiferenciaOpinion())
				a=1;
		}
		return a;
	}

}

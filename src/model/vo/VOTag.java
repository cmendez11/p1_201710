package model.vo;

public class VOTag implements Comparable<VOTag>{
	private long idPelicula;
	private String tag;
	private long timestamp;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	//
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}

	@Override
	public int compareTo(VOTag o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public int comparar(VOTag o, int tipo)
	{
		int a = 0;

		if(tipo == 0)
		{
			if(idPelicula < o.getIdPelicula())
				a=-1;
			else if(idPelicula > o.getIdPelicula())
				a=1;
		}
		else if(tipo == 1)
		{
				a = tag.compareToIgnoreCase(o.getTag());
		}
		else if(tipo ==2)
		{
			if(timestamp < o.getTimestamp())
				a=-1;
			else if(timestamp > o.getTimestamp())
				a=1;
		}
		return a;
	}

}

package view;

import java.util.Comparator;
import java.util.Scanner;

import model.data_structures.ArrayX;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.Lista;
import model.data_structures.Mer;
import model.data_structures.Merge;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.CopyOfSistemaRecomendacionPeliculas;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;


public class Vista {
	private static  Queue cola;
	private static  Stack pila;
	private Merge merg;
	//static SistemaRecomendacionPeliculas s;
	static SistemaRecomendacionPeliculas s;
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		long xdxd = 0;
		s=new SistemaRecomendacionPeliculas();
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:
				s.cargarPeliculasSR("data/movies.csv");
				s.cargarTagsSR("data/tags.csv");
				s.cargarRatingsSR("data/ratings.csv");


				s.agregarOperacionSR("cargar", xdxd, xdxd);System.out.println(s.pila.size());
				break;
			case 2:

				System.out.println(":::"+s.pelicula.darElemento(9124).getTitulo());
				System.out.println(":::"+s.pelicula.darElemento(9124).getGenerosAsociados().darElemento(0));
				System.out.println(":::");
				System.out.println(":::"+s.rating.darElemento(100003).getIdPelicula());
				System.out.println(":::"+s.tag.darElemento(1295).getTag());
				System.out.println(":::");
				System.out.println(":::"+s.pelicula.darElemento(0).getTitulo());
				System.out.println(":::"+s.pelicula.darElemento(0).getGenerosAsociados().darElemento(0));
				System.out.println(":::");
				System.out.println(":::"+s.rating.darElemento(0).getIdPelicula());
				System.out.println(":::"+s.tag.darElemento(0).getTag());

				s.agregarOperacionSR("buscar", xdxd, xdxd);
				break;
			case 3:
				Lista mm=(Lista) s.peliculasPopularesSR(5);
				for(int i=0;i<mm.darNumeroElementos();i++)
				{
					System.out.println(i+":::"+((VOGeneroPelicula)mm.darElemento(i)).getGenero());
					for(int o=0;o<((VOGeneroPelicula)mm.darElemento(i)).getPeliculas().darNumeroElementos();o++)
					{
						System.out.println(i+":::"+((VOGeneroPelicula)mm.darElemento(i)).getPeliculas().darElemento(o).getNumeroRatings()+":::"+((VOGeneroPelicula)mm.darElemento(i)).getPeliculas().darElemento(o).getPromedioRatings());

					}
				}


				/*for(int i=0;i<s.rating.darNumeroElementos();i++)
				{
					System.out.println(i+":::"+s.rating.darElemento(i).getIdPelicula());

				}*/
				/*for(int i=0;i<s.pelicula.darNumeroElementos();i++)
				{
					if(s.pelicula.darElemento(i).getIdUsuario()==163949)
					System.out.println(i+":::"+s.pelicula.darElemento(i).getIdUsuario()+":::"+s.pelicula.darElemento(i).getNumeroRatings());

				}*/
				/*for(int i=0;i<248;i++)
				{
					System.out.println(i+":::"+s.pelicula.darElemento(i).getIdUsuario()+":::"+s.pelicula.darElemento(i).getNumeroRatings()+":::"+s.rating.darElemento(i).getIdPelicula());

				}*/

				s.agregarOperacionSR("Promedio", xdxd, xdxd);
				break;
			case 4:


				/*Merge x=new Merge();
				x.mergesort(s.pelicula,new Comparator<VOPelicula>() {

					@Override
					public int compare(VOPelicula o1, VOPelicula o2) {


						return o1.getTitulo().compareTo(o2.getTitulo());
					}

				});*/
				/*Merge<VOPelicula> x = new Merge<VOPelicula>();
				ILista<VOPelicula> list = s.pelicula;
				x.mergesort(list);


				s.peliculasPopularesSR(0);
				for(int i=0;i<s.pelicula.darNumeroElementos();i++)
				{
					System.out.println(i+":::"+s.pelicula.darElemento(i).getTitulo());

				}*/

				s.cargarPeliculasSR("data/movies.csv");
				s.cargarTagsSR("data/tags.csv");
				s.cargarRatingsSR("data/ratings.csv");



				Merge<VORating> R = new Merge<VORating>();
				ArrayX<VORating> arregloR = s.rating;
				//EN ESTE LUGAR ES DONDE RECIBIRA UN PARAMETRO PARA ORDENAR, SI PUEDE HAGALO CON UN ESPECIE DE SWITCH
				//COMO EN LE QUE SE RECIBE ESTO  (eSPECIFICAMENTE DONDE SALE EL N�MERO 2
				R.mergesortC(arregloR, 2);

				System.out.println("Esta ordenado");

				for(int i=0;i<s.rating.darNumeroElementos();i++)
				{
					System.out.println(i+":::"+s.rating.darElemento(i).getRating());

				}

				Merge<VOPelicula> P = new Merge<VOPelicula>();
				ArrayX<VOPelicula> arregloP = s.pelicula;
				//EN ESTE LUGAR ES DONDE RECIBIRA UN PARAMETRO PARA ORDENAR, SI PUEDE HAGALO CON UN ESPECIE DE SWITCH
				//COMO EN LE QUE SE RECIBE ESTO  (eSPECIFICAMENTE DONDE SALE EL N�MERO 2
				P.mergesortA(arregloP, 0);

				System.out.println("Esta ordenado");

				for(int i=0;i<s.pelicula.darNumeroElementos();i++)
				{
					System.out.println(i+":::"+s.pelicula.darElemento(i).getTitulo());

				}

				Merge<VOTag> T = new Merge<VOTag>();
				ArrayX<VOTag> arregloT = s.tag;
				//EN ESTE LUGAR ES DONDE RECIBIRA UN PARAMETRO PARA ORDENAR, SI PUEDE HAGALO CON UN ESPECIE DE SWITCH
				//COMO EN LE QUE SE RECIBE ESTO  (eSPECIFICAMENTE DONDE SALE EL N�MERO 2
				T.mergesortB(arregloT, 1);

				System.out.println("Esta ordenado");

				for(int i=0;i<s.tag.darNumeroElementos();i++)
				{
					System.out.println(i+":::"+s.tag.darElemento(i).getTag());

				}

				s.agregarOperacionSR("Cargar y Ordenar", xdxd, xdxd);
				break;
			case 5:
				ArrayX aee=(ArrayX) s.catalogoPeliculasOrdenadoSR();
				for(int i=0;i<aee.darNumeroElementos();i++)
				{

					VOPelicula m=(VOPelicula) aee.darElemento(i);
					System.out.println(i+":::"+m.getAgnoPublicacion()+"::::"+m.getTitulo()+"::::"+m.getPromedioRatings());
				}

				break;
			case 6:
				//recomendar por genero
				Lista ae=(Lista) s.recomendarGeneroSR();
				for(int i=0;i<ae.darNumeroElementos();i++)
				{
					System.out.println(":::"+((VOGeneroPelicula) ae.darElemento(i)).getGenero());
					ILista<VOPelicula> m=((VOGeneroPelicula) ae.darElemento(i)).getPeliculas();
					for(int o=0;o<m.darNumeroElementos();o++)
					{
						VOPelicula ma=(VOPelicula) m.darElemento(o);
						System.out.println(i+":::"+ma.getAgnoPublicacion()+"::::"+ma.getTitulo()+"::::"+ma.getPromedioRatings());
					}

				}
				System.out.println(":::"+ae.darNumeroElementos());
				break;
			case 7:
				//opinion por genero
				Lista axx=(Lista) s.recomendarGeneroSR();
				for(int i=0;i<axx.darNumeroElementos();i++)
				{
					System.out.println(":::"+((VOGeneroPelicula) axx.darElemento(i)).getGenero());
					ILista<VOPelicula> m=((VOGeneroPelicula) axx.darElemento(i)).getPeliculas();
					for(int o=0;o<m.darNumeroElementos();o++)
					{
						VOPelicula ma=(VOPelicula) m.darElemento(o);
						System.out.println(i+":::"+ma.getAgnoPublicacion()+"::::"+ma.getTitulo()+"::::"+ma.getPromedioRatings());
					}

				}
				System.out.println(":::"+axx.darNumeroElementos());
				break;
			case 8:
				//rating pelicula
				ArrayX ratpeli= (ArrayX) s.ratingsPeliculaSR(1);
				for(int o=0;o<ratpeli.darNumeroElementos();o++)
				{

					System.out.println(o+":::"+((VORating)ratpeli.darElemento(o)).getIdPelicula()+":::"+((VORating)ratpeli.darElemento(o)).getIdUsuario());
				}
				break;
			case 9:
				//catalogo usarios ordenados
				ArrayX usORde= (ArrayX) s.catalogoUsuariosOrdenadoSR();
				for(int o=0;o<usORde.darNumeroElementos();o++)
				{

					System.out.println(o+":::"+((VOUsuario)usORde.darElemento(o)).getPrimerTimestamp()+":::"+((VOUsuario)usORde.darElemento(o)).getNumRatings()+":::"+((VOUsuario)usORde.darElemento(o)).getIdUsuario());
				}
				break;
			case 10:
				//recomendar tag genero
				Lista ORde=  (Lista) s.recomendarTagsGeneroSR(5);
				for(int o=0;o<ORde.darNumeroElementos();o++)
				{
					VOGeneroPelicula vo=(VOGeneroPelicula) ORde.darElemento(o);
					System.out.println(":::"+vo.getGenero());
					for(int i=0;i<vo.getPeliculas().darNumeroElementos();i++)
					{
						
						System.out.println(i+":::"+vo.getPeliculas().darElemento(i).getNumeroTags()+":::"+vo.getPeliculas().darElemento(i).getTitulo());
					}
					
				}
				System.out.println(":::"+((VOGeneroPelicula)ORde.darElemento(0)).getPeliculas().darNumeroElementos());
				break;
			case 20:
				int zz = s.tagsPeliculaSR(164979).darNumeroElementos();
				for(int i = 0; i < zz; i++)
				{
					System.out.println(""+s.tagsPeliculaSR(164979).darElemento(i).getTag());
				}
				break;


			case 21:

				for (int i = 0; i < s.darHistoralOperacionesSR().darNumeroElementos(); i++) 
				{
					System.out.println(""+s.darHistoralOperacionesSR().darElemento(i));
				}
				break;
			case 22:
				s.darUltimasOperaciones(5);
				for (int i = 0; i < s.darUltimasOperaciones(5).darNumeroElementos(); i++) 
				{
					System.out.println(""+s.darUltimasOperaciones(5).darElemento(i));
				}
				break;

			}
		}


	}
	private static void printMenu() {

		System.out.println("1. Cree una nueva colecci�n de pel�culas (data/movies.csv)");
		System.out.println("2. Buscar pel�culas por subcadena");


	}

}
